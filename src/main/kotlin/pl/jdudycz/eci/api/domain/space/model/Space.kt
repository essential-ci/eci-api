package pl.jdudycz.eci.api.domain.space.model

import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Space(
        var id: String,
        val name: String,
        val agentRegistrationCode: String,
)
