package pl.jdudycz.eci.api.domain.agent.core

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.common.util.MonoEither
import pl.jdudycz.eci.api.common.SynchronousExecutionWrapper
import pl.jdudycz.eci.api.domain.agent.model.RegistrationRequest
import pl.jdudycz.eci.common.domain.Agent.AgentData
import pl.jdudycz.eci.common.domain.Agent.AgentRegistrationFailure
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.agentRegister
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import reactor.core.publisher.Mono

@Service
class AgentRegisterTemplate(publisher: EventPublisher,
                            @Qualifier("agentRegisterSuccess") successConsumer: EventConsumer,
                            @Qualifier("agentRegisterFailed") failureConsumer: EventConsumer)
    : SynchronousExecutionWrapper(publisher, successConsumer, failureConsumer) {

    fun register(request: RegistrationRequest): MonoEither<String, String> = request.run {
            Mono.fromCallable { agentRegister(registrationCode, publicIp, name, apiKey) }.transform(::publishCommand)
    }

    override fun parseSuccessResult(event: Event): Either<Nothing, String> =
            AgentData.parseFrom(event.data).id.let(::Right)

    override fun parseFailureResult(event: Event): Either<String, Nothing> =
            AgentRegistrationFailure.parseFrom(event.data).errorMessage.let(::Left)
}
