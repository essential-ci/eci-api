package pl.jdudycz.eci.api.domain.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.api.domain.job.persistence.model.TaskOutput
import reactor.core.publisher.Flux

@Repository
interface TaskOutputRepository : ReactiveCrudRepository<TaskOutput, String> {

    fun findAllByTaskId(taskId: String): Flux<TaskOutput>
}
