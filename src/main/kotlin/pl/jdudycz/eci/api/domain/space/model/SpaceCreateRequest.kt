package pl.jdudycz.eci.api.domain.space.model

data class SpaceCreateRequest(val name: String)
