package pl.jdudycz.eci.api.domain.project

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.*
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class ProjectConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun projectSaveSuccess(): EventConsumer = EventConsumer.create(PROJECT_SAVE_SUCCESS_TOPIC, kafkaProperties)

    @Bean
    fun projectSaveFailed(): EventConsumer = EventConsumer.create(PROJECT_SAVE_FAILED_TOPIC, kafkaProperties)
}
