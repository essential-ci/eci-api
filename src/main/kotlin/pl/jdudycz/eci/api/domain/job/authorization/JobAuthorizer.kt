package pl.jdudycz.eci.api.domain.job.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import pl.jdudycz.eci.api.security.authorization.ParamExtractor
import pl.jdudycz.eci.api.security.authorization.RoleAccessManager
import pl.jdudycz.eci.api.domain.space.authorization.SpaceMembershipResolver

@Service
class JobAuthorizer(private val spaceMembershipResolver: SpaceMembershipResolver,
                    private val roleAuthorizer: RoleAccessManager) {

    fun authorizeJobAccess(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.DEVELOPER,
                    spaceMembershipResolver::getJobSpace,
                    JOB_ID_VARIABLE
            )

    fun authorizeTaskAccess(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.DEVELOPER,
                    spaceMembershipResolver::getTaskSpace,
                    TASK_ID_VARIABLE
            )

    companion object {
        private const val JOB_ID_VARIABLE = "jobId"
        private const val TASK_ID_VARIABLE = "taskId"
    }
}
