package pl.jdudycz.eci.api.domain.project.persistence

import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Project(
        var id: String,
        val spaceId: String,
        val name: String,
        val repoUrl: String,
        val variables: Map<String, Any>,
        val secretVariables: Map<String, Any>
)
