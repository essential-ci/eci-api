package pl.jdudycz.eci.api.domain.job

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.domain.job.model.TaskWithOutput
import pl.jdudycz.eci.api.domain.job.persistence.TaskOutputRepository
import pl.jdudycz.eci.api.domain.job.persistence.TaskRepository
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import pl.jdudycz.eci.api.domain.job.persistence.model.TaskOutput
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/api/task")
class TaskController(private val taskRepository: TaskRepository,
                     private val taskOutputRepository: TaskOutputRepository) {

    @GetMapping("all")
    fun getJobTasks(@RequestParam jobId: String): Flux<TaskWithOutput> =
            taskRepository
                    .findAllByJobId(jobId)
                    .flatMap(::fetchOutput)
                    .sort(Comparator.comparing(TaskWithOutput::ordinal))

    @GetMapping("{id}")
    fun getTask(@PathVariable id: String): MonoResponse<TaskWithOutput> =
            taskRepository.findById(id)
                    .flatMap(::fetchOutput)
                    .map { ResponseEntity.ok(it) }
                    .switchIfEmpty(ResponseEntity.notFound().build<TaskWithOutput>().toMono())

    private fun fetchOutput(task: Task): Mono<TaskWithOutput> =
            taskOutputRepository
                    .findAllByTaskId(task.id)
                    .collectList()
                    .map { it.sortedBy(TaskOutput::timestamp) }
                    .map { TaskWithOutput(task, it) }

    @GetMapping("{id}/output")
    fun getTaskOutput(@PathVariable id: String): Flux<TaskOutput> = taskOutputRepository.findAllByTaskId(id)
}
