package pl.jdudycz.eci.api.domain.agent.persistence

import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Agent(
        var id: String,
        val spaceId: String,
        val host: String,
        val name: String,
        val isAvailable: Boolean
)
