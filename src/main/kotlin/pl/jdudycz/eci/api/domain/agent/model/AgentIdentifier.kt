package pl.jdudycz.eci.api.domain.agent.model

data class AgentIdentifier(val agentId: String, val apiKey: String)
