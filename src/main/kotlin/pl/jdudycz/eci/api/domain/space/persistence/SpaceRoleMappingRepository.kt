package pl.jdudycz.eci.api.domain.space.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.api.domain.space.model.SpaceRoleMapping
import reactor.core.publisher.Flux

@Repository
interface SpaceRoleMappingRepository : ReactiveCrudRepository<SpaceRoleMapping, String> {

    fun findByUserIdAndSpaceId(userId: String, spaceId: String): Flux<SpaceRoleMapping>

    fun findByUserId(userId: String): Flux<SpaceRoleMapping>

    fun findBySpaceId(spaceId: String): Flux<SpaceRoleMapping>
}
