package pl.jdudycz.eci.api.domain.agent

import arrow.core.Either
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.domain.agent.core.AgentRegisterTemplate
import pl.jdudycz.eci.api.domain.agent.model.RegistrationRequest
import pl.jdudycz.eci.api.domain.agent.persistence.Agent
import pl.jdudycz.eci.api.domain.agent.persistence.AgentRepository
import pl.jdudycz.eci.common.domain.event.agentUnregister
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/api/agent")
class AgentController(private val agentRepository: AgentRepository,
                      private val publisher: EventPublisher,
                      private val agentRegisterer: AgentRegisterTemplate) {

    @PostMapping("register")
    fun registerAgent(@RequestBody body: RegistrationRequest): MonoResponse<String> =
            agentRegisterer.register(body).map(::composeResult)

    private fun composeResult(result: Either<String, String>) =
            result.fold({ ResponseEntity.badRequest().body(it) }, { ResponseEntity.ok(it) })

    @GetMapping("all")
    fun getAgents(@RequestParam spaceId: String): Flux<Agent> = agentRepository.findBySpaceId(spaceId)

    @GetMapping("{agentId}")
    fun getAgent(@PathVariable agentId: String): MonoResponse<Agent> =
            agentRepository.findById(agentId)
                    .map { ResponseEntity.ok(it) }
                    .switchIfEmpty(ResponseEntity.notFound().build<Agent>().toMono())

    @DeleteMapping("{agentId}")
    fun unregisterAgent(@PathVariable agentId: String): MonoResponse<Void> =
            agentRepository.findById(agentId)
                    .switchIfEmpty(Mono.error(Exception()))
                    .map { agentUnregister(it.id) }
                    .transform(publisher::publish)
                    .doOnNext { log.debug("Unregister command for agent $agentId published") }
                    .map { ResponseEntity.accepted().build<Void>() }
                    .onErrorResume { ResponseEntity.notFound().build<Void>().toMono() }

    companion object {
        private val log = logger<AgentController>()
    }

}
