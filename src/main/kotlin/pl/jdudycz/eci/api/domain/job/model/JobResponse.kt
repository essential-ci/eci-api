package pl.jdudycz.eci.api.domain.job.model

import pl.jdudycz.eci.api.domain.job.persistence.model.ExecutionStatus
import pl.jdudycz.eci.api.domain.job.persistence.model.Job
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import pl.jdudycz.eci.api.domain.job.persistence.model.TriggerType
import java.time.Instant

data class JobResponse(
        val id: String,
        val projectId: String,
        val timeCreated: Instant,
        val timeStarted: Instant?,
        val timeFinished: Instant?,
        val triggerType: TriggerType,
        val agentId: String?,
        val refValue: String,
        val commitSha: String,
        val status: ExecutionStatus,
        val tasks: List<TaskResponse>,
        val errorMessage: String?
) {
    constructor(job: Job, tasks: List<Task>) : this(
            job.id,
            job.projectId,
            job.timeCreated,
            job.timeStarted,
            job.timeFinished,
            job.triggerType,
            job.agentId,
            job.refValue,
            job.commitSha,
            job.status,
            tasks.map(::TaskResponse),
            job.errorMessage
    )
}
