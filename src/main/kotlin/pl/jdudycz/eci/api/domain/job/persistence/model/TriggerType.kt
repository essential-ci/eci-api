package pl.jdudycz.eci.api.domain.job.persistence.model

enum class TriggerType {
    BRANCH,
    TAG
}
