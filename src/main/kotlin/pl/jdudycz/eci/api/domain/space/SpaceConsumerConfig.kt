package pl.jdudycz.eci.api.domain.space

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.SPACE_SAVE_FAILED_TOPIC
import pl.jdudycz.eci.common.kafka.SPACE_SAVE_SUCCESS_TOPIC
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class SpaceConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun spaceSaveSuccess(): EventConsumer = EventConsumer.create(SPACE_SAVE_SUCCESS_TOPIC, kafkaProperties)

    @Bean
    fun spaceSaveFailed(): EventConsumer = EventConsumer.create(SPACE_SAVE_FAILED_TOPIC, kafkaProperties)
}
