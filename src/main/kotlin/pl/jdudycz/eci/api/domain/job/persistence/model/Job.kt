package pl.jdudycz.eci.api.domain.job.persistence.model

import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Job(
        val id: String,
        val projectId: String,
        val timeCreated: Instant,
        val timeStarted: Instant?,
        val timeFinished: Instant?,
        val triggerType: TriggerType,
        val agentId: String?,
        val refValue: String,
        val commitSha: String,
        val status: ExecutionStatus,
        val errorMessage: String?)
