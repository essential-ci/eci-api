package pl.jdudycz.eci.api.domain.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import pl.jdudycz.eci.api.domain.job.persistence.model.Job
import reactor.core.publisher.Flux

interface JobRepository : ReactiveCrudRepository<Job, String> {

    fun findByProjectId(projectId: String): Flux<Job>
}
