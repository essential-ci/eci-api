package pl.jdudycz.eci.api.domain.agent.authorization

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import pl.jdudycz.eci.api.domain.space.authorization.SpaceAuthorizer
import pl.jdudycz.eci.api.security.authorization.AuthorizationConfig
import pl.jdudycz.eci.api.security.authorization.queryParamExtractor
import pl.jdudycz.eci.api.security.authorization.variableExtractor

@Configuration
class AgentAuthorizationConfig(private val agentAuthorizer: AgentAuthorizer,
                               private val spaceAuthorizer: SpaceAuthorizer) : AuthorizationConfig {

    override fun configureAccess(spec: AuthorizeExchangeSpec): AuthorizeExchangeSpec =
            spec.pathMatchers("/api/agent/all")
                    .access(spaceAuthorizer.authorizeRead(::queryParamExtractor))
                    .pathMatchers(HttpMethod.GET, "/api/agent/{agentId}")
                    .access(agentAuthorizer.authorizeRead(::variableExtractor))
                    .pathMatchers(HttpMethod.DELETE, "/api/agent/{agentId}")
                    .access(agentAuthorizer.authorizeWrite(::variableExtractor))
                    .pathMatchers("/api/agent/register").permitAll()
                    .pathMatchers("/api/ws/agent").permitAll()
}
