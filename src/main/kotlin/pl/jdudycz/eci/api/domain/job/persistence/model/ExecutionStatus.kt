package pl.jdudycz.eci.api.domain.job.persistence.model

enum class ExecutionStatus{
    PENDING,
    SUCCEEDED,
    RUNNING,
    FAILED,
    CANCELLED
}
