package pl.jdudycz.eci.api.domain.job.authorization

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import pl.jdudycz.eci.api.domain.project.authorization.ProjectAuthorizer
import pl.jdudycz.eci.api.security.authorization.AuthorizationConfig
import pl.jdudycz.eci.api.security.authorization.queryParamExtractor
import pl.jdudycz.eci.api.security.authorization.variableExtractor

@Configuration
class JobAuthorizationConfig(private val jobAuthorizer: JobAuthorizer,
                             private val projectAuthorizer: ProjectAuthorizer): AuthorizationConfig {

    override fun configureAccess(spec: AuthorizeExchangeSpec): AuthorizeExchangeSpec =
            spec.pathMatchers("/api/job/all")
                    .access(projectAuthorizer.authorizeRead(::queryParamExtractor))
                    .pathMatchers("/api/job/{jobId}/**")
                    .access(jobAuthorizer.authorizeJobAccess(::variableExtractor))
                    .pathMatchers("/api/task/all")
                    .access(jobAuthorizer.authorizeJobAccess(::queryParamExtractor))
                    .pathMatchers("/api/task/{taskId}/**")
                    .access(jobAuthorizer.authorizeTaskAccess(::variableExtractor))
}
