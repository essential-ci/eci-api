package pl.jdudycz.eci.api.domain.agent.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import pl.jdudycz.eci.api.security.authorization.ParamExtractor
import pl.jdudycz.eci.api.security.authorization.RoleAccessManager
import pl.jdudycz.eci.api.domain.space.authorization.SpaceMembershipResolver

@Service
class AgentAuthorizer(private val spaceMembershipResolver: SpaceMembershipResolver,
                      private val roleAuthorizer: RoleAccessManager) {

    fun authorizeWrite(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.ADMIN,
                    spaceMembershipResolver::getAgentSpace,
                    AGENT_ID_VARIABLE
            )

    fun authorizeRead(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.DEVELOPER,
                    spaceMembershipResolver::getAgentSpace,
                    AGENT_ID_VARIABLE
            )

    companion object {
        private const val AGENT_ID_VARIABLE = "agentId"
    }
}
