package pl.jdudycz.eci.api.domain.project.model

import pl.jdudycz.eci.api.domain.project.persistence.Project


data class ProjectResponse(
        var id: String,
        val spaceId: String,
        val name: String,
        val repoUrl: String,
        val variables: Map<String, Any>,
        val secretVariablesNames: Set<String>
) {
    constructor(project: Project) : this(
            project.id,
            project.spaceId,
            project.name,
            project.repoUrl,
            project.variables,
            project.secretVariables.keys
    )
}
