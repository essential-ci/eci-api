package pl.jdudycz.eci.api.domain.space.model

data class AccessRemoveRequest(val userId: String)
