package pl.jdudycz.eci.api.domain.space.core

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.common.util.MonoEither
import pl.jdudycz.eci.api.common.SynchronousExecutionWrapper
import pl.jdudycz.eci.common.domain.Space
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.spaceSave
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import reactor.core.publisher.Mono

@Service
class SpaceSaveTemplate(publisher: EventPublisher,
                        @Qualifier("spaceSaveSuccess") successConsumer: EventConsumer,
                        @Qualifier("spaceSaveFailed") failureConsumer: EventConsumer)
    : SynchronousExecutionWrapper(publisher, successConsumer, failureConsumer) {

    fun save(principal: String, name: String, spaceId: String? = null): MonoEither<String, String> =
            Mono.fromCallable { constructSpaceData(principal, name, spaceId) }
                    .map(::spaceSave)
                    .transform(::publishCommand)

    private fun constructSpaceData(principal: String, name: String, spaceId: String? = null): Space.SpaceData =
            Space.SpaceData.newBuilder()
                    .setId(spaceId ?: "")
                    .setName(name)
                    .setPrincipal(principal)
                    .build()

    override fun parseSuccessResult(event: EventOuterClass.Event): Either<Nothing, String> =
            Space.SpaceData.parseFrom(event.data).id.let(::Right)

    override fun parseFailureResult(event: EventOuterClass.Event): Either<String, Nothing> =
            Space.SpaceCreateFailure.parseFrom(event.data).errorMessage.let(::Left)
}
