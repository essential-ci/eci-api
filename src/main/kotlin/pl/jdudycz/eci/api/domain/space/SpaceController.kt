package pl.jdudycz.eci.api.domain.space

import arrow.core.Either
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.common.util.asDefaultResponse
import pl.jdudycz.eci.api.domain.space.core.SpaceSaveTemplate
import pl.jdudycz.eci.api.domain.space.model.*
import pl.jdudycz.eci.api.domain.space.persistence.SpaceRepository
import pl.jdudycz.eci.api.domain.space.persistence.SpaceRoleMappingRepository
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.accessGrant
import pl.jdudycz.eci.common.domain.event.accessRemove
import pl.jdudycz.eci.common.domain.event.spaceRemove
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.Principal

@RestController
@RequestMapping("/api/space")
class SpaceController(private val spaceRepository: SpaceRepository,
                      private val roleRepository: SpaceRoleMappingRepository,
                      private val publisher: EventPublisher,
                      private val spaceSave: SpaceSaveTemplate) {

    @PostMapping
    fun createSpace(principal: Principal, @RequestBody body: SpaceCreateRequest): MonoResponse<String> =
            saveSpace(principal.name, body.name)

    @GetMapping("all")
    fun getSpaces(principal: Principal): Flux<Space> =
            roleRepository
                    .findByUserId(principal.name)
                    .map(SpaceRoleMapping::spaceId)
                    .transform(spaceRepository::findAllById)

    @GetMapping("{spaceId}")
    fun getSpace(@PathVariable spaceId: String): MonoResponse<Space> =
            spaceRepository.findById(spaceId)
                    .map { ResponseEntity.ok(it) }
                    .switchIfEmpty(ResponseEntity.notFound().build<Space>().toMono())

    @DeleteMapping("{spaceId}")
    fun removeSpace(@PathVariable spaceId: String): Mono<ResponseEntity<Void>> =
            publishIfSpaceExist(spaceId, spaceRemove(spaceId))
                    .doOnNext { log.debug("Remove space command for space $spaceId published") }

    @PutMapping("{spaceId}")
    fun updateSpace(principal: Principal,
                    @RequestBody body: SpaceCreateRequest,
                    @PathVariable spaceId: String): MonoResponse<String> =
            spaceRepository.findById(spaceId)
                    .flatMap { saveSpace(principal.name, body.name, spaceId) }
                    .switchIfEmpty(ResponseEntity.notFound().build<String>().toMono())

    private fun saveSpace(principal: String, name: String, spaceId: String? = null) =
            spaceSave.save(principal, name, spaceId).map(Either<String, String>::asDefaultResponse)

    @GetMapping("{spaceId}/access")
    fun getRoles(@PathVariable spaceId: String): Flux<SpaceRoleMapping> = roleRepository.findBySpaceId(spaceId)

    @PostMapping("{spaceId}/access")
    fun grantAccess(@PathVariable spaceId: String, @RequestBody body: AccessGrantRequest): MonoResponse<Void> =
            publishIfSpaceExist(spaceId, accessGrant(spaceId, body.userId, body.role.asEventPayload()))
                    .doOnNext { log.debug("Grant access command for space $spaceId and user ${body.userId} published") }

    @DeleteMapping("{spaceId}/access")
    fun removeAccess(@PathVariable spaceId: String, @RequestBody body: AccessRemoveRequest): MonoResponse<Void> =
            publishIfSpaceExist(spaceId, accessRemove(spaceId, body.userId))
                    .doOnNext { log.debug("Remove access command for space $spaceId and user ${body.userId} published") }

    fun publishIfSpaceExist(spaceId: String, event: EventOuterClass.Event): MonoResponse<Void> =
            spaceRepository.findById(spaceId)
                    .switchIfEmpty(Mono.error(Exception()))
                    .flatMap { publisher.publish(event.toMono()) }
                    .map { ResponseEntity.accepted().build<Void>() }
                    .onErrorResume { ResponseEntity.notFound().build<Void>().toMono() }

    companion object{
        private val log = logger<SpaceController>()
    }
}
