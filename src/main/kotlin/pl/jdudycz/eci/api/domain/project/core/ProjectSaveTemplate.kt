package pl.jdudycz.eci.api.domain.project.core

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.common.util.MonoEither
import pl.jdudycz.eci.api.common.SynchronousExecutionWrapper
import pl.jdudycz.eci.api.domain.project.model.ProjectSaveRequest
import pl.jdudycz.eci.common.domain.Project
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.projectSave
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.encryption.TextEncryptor
import reactor.core.publisher.Mono

@Service
class ProjectSaveTemplate(@Qualifier("projectSaveSuccess") successConsumer: EventConsumer,
                          @Qualifier("projectSaveFailed") failureConsumer: EventConsumer,
                          publisher: EventPublisher,
                          private val encryptor: TextEncryptor)
    : SynchronousExecutionWrapper(publisher, successConsumer, failureConsumer) {

    fun save(data: ProjectSaveRequest, id: String? = null): MonoEither<String, String> =
            Mono.fromCallable { projectSave(data.toCommandData(id, encryptor::encrypt)) }.transform(::publishCommand)

    override fun parseSuccessResult(event: EventOuterClass.Event): Either<Nothing, String> =
            Project.ProjectData.parseFrom(event.data).id.let(::Right)

    override fun parseFailureResult(event: EventOuterClass.Event): Either<String, Nothing> =
            Project.ProjectCreateFailure.parseFrom(event.data).errorMessage.let(::Left)
}
