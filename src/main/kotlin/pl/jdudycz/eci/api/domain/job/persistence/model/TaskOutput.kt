package pl.jdudycz.eci.api.domain.job.persistence.model

import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class TaskOutput(
        val taskId: String,
        val timestamp: Instant,
        val content: String
)
