package pl.jdudycz.eci.api.domain.project.authorization

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import pl.jdudycz.eci.api.domain.space.authorization.SpaceAuthorizer
import pl.jdudycz.eci.api.security.authorization.AuthorizationConfig
import pl.jdudycz.eci.api.security.authorization.queryParamExtractor
import pl.jdudycz.eci.api.security.authorization.variableExtractor

@Configuration
class ProjectAuthorizationConfig(private val projectAuthorizer: ProjectAuthorizer,
                                 private val spaceAuthorizer: SpaceAuthorizer) : AuthorizationConfig {

    override fun configureAccess(spec: AuthorizeExchangeSpec): AuthorizeExchangeSpec =
            spec.pathMatchers("/api/project/all")
                    .access(spaceAuthorizer.authorizeRead(::queryParamExtractor))
                    .pathMatchers(HttpMethod.GET, "/api/project/{projectId}")
                    .access(projectAuthorizer.authorizeRead(::variableExtractor))
                    .pathMatchers("/api/project/{projectId}")
                    .access(projectAuthorizer.authorizeWrite(::variableExtractor))
}
