package pl.jdudycz.eci.api.domain.job.model

import pl.jdudycz.eci.api.domain.job.persistence.model.ExecutionStatus
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import pl.jdudycz.eci.api.domain.job.persistence.model.TaskOutput
import java.time.Instant

data class TaskResponse(
        val id: String,
        val jobId: String,
        val ordinal: Int,
        val name: String,
        val image: String,
        val variables: Map<String, Any>,
        val secretVariablesNames: Set<String>,
        val commands: List<String>,
        val status: ExecutionStatus,
        val timeStarted: Instant?,
        val timeFinished: Instant?
) {
    constructor(task: Task) : this(
            task.id,
            task.jobId,
            task.ordinal,
            task.name,
            task.image,
            task.variables,
            task.secretVariables.keys,
            task.commands,
            task.status,
            task.timeStarted,
            task.timeFinished
    )
}
