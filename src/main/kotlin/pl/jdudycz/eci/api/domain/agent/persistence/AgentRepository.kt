package pl.jdudycz.eci.api.domain.agent.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface AgentRepository : ReactiveCrudRepository<Agent, String> {

    fun findBySpaceId(spaceId: String): Flux<Agent>
}
