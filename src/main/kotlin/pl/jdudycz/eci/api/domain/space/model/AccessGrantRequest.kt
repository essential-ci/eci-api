package pl.jdudycz.eci.api.domain.space.model

data class AccessGrantRequest(val userId: String, val role: SpaceRole)
