package pl.jdudycz.eci.api.domain.space.authorization

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import pl.jdudycz.eci.api.security.authorization.AuthorizationConfig
import pl.jdudycz.eci.api.security.authorization.variableExtractor

@Configuration
class SpaceAuthorizationConfig(private val spaceAuthorizer: SpaceAuthorizer) : AuthorizationConfig {

    override fun configureAccess(spec: AuthorizeExchangeSpec): AuthorizeExchangeSpec =
            spec.pathMatchers("/api/space/all")
                    .authenticated()
                    .pathMatchers(HttpMethod.GET, "/api/space/{spaceId}/**")
                    .access(spaceAuthorizer.authorizeRead(::variableExtractor))
                    .pathMatchers("/api/space/{spaceId}/**")
                    .access(spaceAuthorizer.authorizeWrite(::variableExtractor))
}
