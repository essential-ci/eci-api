package pl.jdudycz.eci.api.domain.space.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import pl.jdudycz.eci.api.domain.space.model.Space

interface SpaceRepository : ReactiveCrudRepository<Space, String>
