package pl.jdudycz.eci.api.domain.job.persistence.model

import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Task(val id: String,
                val jobId: String,
                val ordinal: Int,
                val name: String,
                val image: String,
                val variables: Map<String, Any>,
                val secretVariables: Map<String, Any>,
                val commands: List<String>,
                val status: ExecutionStatus,
                val timeStarted: Instant?,
                val timeFinished: Instant?)
