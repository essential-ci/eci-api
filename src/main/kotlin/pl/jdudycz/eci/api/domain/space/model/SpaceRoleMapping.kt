package pl.jdudycz.eci.api.domain.space.model

data class SpaceRoleMapping(val userId: String, val spaceId: String, val role: SpaceRole)
