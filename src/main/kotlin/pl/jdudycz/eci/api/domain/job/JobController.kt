package pl.jdudycz.eci.api.domain.job

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.domain.job.model.JobResponse
import pl.jdudycz.eci.api.domain.job.persistence.JobRepository
import pl.jdudycz.eci.api.domain.job.persistence.TaskRepository
import pl.jdudycz.eci.api.domain.job.persistence.model.Job
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import pl.jdudycz.eci.common.domain.event.jobCancel
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/api/job")
class JobController(private val jobRepository: JobRepository,
                    private val taskRepository: TaskRepository,
                    private val eventPublisher: EventPublisher) {

    @GetMapping("all")
    fun getProjectJobs(@RequestParam projectId: String): Flux<JobResponse> =
            jobRepository
                    .findByProjectId(projectId)
                    .flatMap(::fetchTasks)
                    .sort(compareByDescending(JobResponse::timeCreated))

    @GetMapping("{id}")
    fun getJob(@PathVariable id: String): MonoResponse<JobResponse> =
            jobRepository.findById(id)
                    .flatMap(::fetchTasks)
                    .map { ResponseEntity.ok(it) }
                    .switchIfEmpty(ResponseEntity.notFound().build<JobResponse>().toMono())

    private fun fetchTasks(job: Job): Mono<JobResponse> =
            taskRepository
                    .findAllByJobId(job.id)
                    .collectList()
                    .map { it.sortedBy(Task::ordinal) }
                    .map { JobResponse(job, it) }

    @PostMapping("{id}/cancel")
    fun cancelJob(@PathVariable id: String): MonoResponse<Void> =
            jobRepository.findById(id)
                    .flatMap {
                        Mono.fromCallable { jobCancel(id) }
                                .transform(eventPublisher::publish)
                    }
                    .doOnNext { log.debug("Cancel command for job $id published") }
                    .map { ResponseEntity.accepted().build<Void>() }
                    .switchIfEmpty(ResponseEntity.notFound().build<Void>().toMono())

    companion object {
        private val log = logger<JobController>()
    }
}
