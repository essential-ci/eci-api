package pl.jdudycz.eci.api.domain.agent.model

data class RegistrationRequest(val name: String,
                               val registrationCode: String,
                               val publicIp: String,
                               val apiKey: String)
