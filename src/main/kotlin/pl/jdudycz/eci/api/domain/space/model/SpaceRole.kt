package pl.jdudycz.eci.api.domain.space.model

import pl.jdudycz.eci.common.domain.Space

enum class SpaceRole(val priority: Int) {
    ADMIN(0),
    DEVELOPER(1);

    fun asEventPayload(): Space.SpaceRole = Space.SpaceRole.valueOf(this.toString())
}
