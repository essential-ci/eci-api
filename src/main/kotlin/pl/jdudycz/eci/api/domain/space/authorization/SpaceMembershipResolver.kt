package pl.jdudycz.eci.api.domain.space.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.agent.persistence.Agent
import pl.jdudycz.eci.api.domain.agent.persistence.AgentRepository
import pl.jdudycz.eci.api.domain.job.persistence.JobRepository
import pl.jdudycz.eci.api.domain.job.persistence.TaskRepository
import pl.jdudycz.eci.api.domain.job.persistence.model.Job
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import pl.jdudycz.eci.api.domain.project.persistence.Project
import pl.jdudycz.eci.api.domain.project.persistence.ProjectRepository
import reactor.core.publisher.Mono

@Service
class SpaceMembershipResolver(private val agentRepository: AgentRepository,
                              private val projectRepository: ProjectRepository,
                              private val jobRepository: JobRepository,
                              private val taskRepository: TaskRepository) {

    fun getTaskSpace(taskId: String): Mono<String> =
            taskRepository.findById(taskId).map(Task::jobId).flatMap(::getJobSpace)

    fun getJobSpace(jobId: String): Mono<String> =
            jobRepository.findById(jobId).map(Job::projectId).flatMap(::getProjectSpace)

    fun getProjectSpace(projectId: String): Mono<String> =
            projectRepository.findById(projectId).map(Project::spaceId)

    fun getAgentSpace(projectId: String): Mono<String> =
            agentRepository.findById(projectId).map(Agent::spaceId)
}
