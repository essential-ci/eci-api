package pl.jdudycz.eci.api.domain.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.api.domain.job.persistence.model.Task
import reactor.core.publisher.Flux

@Repository
interface TaskRepository : ReactiveCrudRepository<Task, String> {

    fun findAllByJobId(jobId: String): Flux<Task>
}
