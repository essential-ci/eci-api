package pl.jdudycz.eci.api.domain.project

import arrow.core.Either
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.common.util.asDefaultResponse
import pl.jdudycz.eci.api.domain.project.core.ProjectSaveTemplate
import pl.jdudycz.eci.api.domain.project.model.ProjectResponse
import pl.jdudycz.eci.api.domain.project.model.ProjectSaveRequest
import pl.jdudycz.eci.api.domain.project.persistence.ProjectRepository
import pl.jdudycz.eci.api.security.authorization.RoleChecker
import pl.jdudycz.eci.common.domain.event.projectRemove
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.Principal


@RestController
@RequestMapping("/api/project")
class ProjectController(private val projectRepository: ProjectRepository,
                        private val publisher: EventPublisher,
                        private val projectSave: ProjectSaveTemplate,
                        private val roleChecker: RoleChecker) {

    @PostMapping
    fun createProject(principal: Principal, @RequestBody body: ProjectSaveRequest): MonoResponse<String> =
            roleChecker.hasAdminRole(principal.name, body.spaceId).flatMap {
                if (it) projectSave.save(body).map(Either<String, String>::asDefaultResponse)
                else Mono.fromCallable { ResponseEntity.status(HttpStatus.FORBIDDEN).build() }
            } .onErrorResume { ResponseEntity.badRequest().build<String>().toMono() }

    @GetMapping("all")
    fun getProjects(@RequestParam spaceId: String): Flux<ProjectResponse> =
            projectRepository.findBySpaceId(spaceId).map(::ProjectResponse)

    @GetMapping("{projectId}")
    fun getProject(@PathVariable projectId: String): MonoResponse<ProjectResponse> =
            projectRepository.findById(projectId)
                    .map(::ProjectResponse)
                    .map { ResponseEntity.ok(it) }
                    .switchIfEmpty(ResponseEntity.notFound().build<ProjectResponse>().toMono())

    @DeleteMapping("{projectId}")
    fun removeProject(@PathVariable projectId: String): Mono<ResponseEntity<Void>> =
            projectRepository.findById(projectId)
                    .switchIfEmpty(Mono.error(Exception()))
                    .map { projectRemove(it.id) }
                    .transform(publisher::publish)
                    .map { ResponseEntity.accepted().build<Void>() }
                    .onErrorResume { ResponseEntity.notFound().build<Void>().toMono() }

    @PutMapping("{projectId}")
    fun updateProject(principal: Principal,
                      @RequestBody body: ProjectSaveRequest,
                      @PathVariable projectId: String): MonoResponse<String> =
            projectRepository.findById(projectId)
                    .flatMap {
                        projectSave.save(body, projectId)
                                .map(Either<String, String>::asDefaultResponse)
                    }
                    .doOnNext { log.debug("Update command for project $projectId published") }
                    .onErrorResume { ResponseEntity.badRequest().build<String>().toMono() }
                    .switchIfEmpty(ResponseEntity.notFound().build<String>().toMono())

    companion object{
        private val log = logger<ProjectController>()
    }
}
