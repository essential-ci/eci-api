package pl.jdudycz.eci.api.domain.agent

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.AGENT_REGISTER_FAILED_TOPIC
import pl.jdudycz.eci.common.kafka.AGENT_REGISTER_SUCCESS_TOPIC
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class AgentConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun agentRegisterSuccess(): EventConsumer = EventConsumer.create(AGENT_REGISTER_SUCCESS_TOPIC, kafkaProperties)

    @Bean
    fun agentRegisterFailed(): EventConsumer = EventConsumer.create(AGENT_REGISTER_FAILED_TOPIC, kafkaProperties)
}
