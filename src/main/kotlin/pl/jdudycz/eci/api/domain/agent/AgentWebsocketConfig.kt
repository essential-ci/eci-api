package pl.jdudycz.eci.api.domain.agent

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.kafka.core.EventPublisher


@Configuration
class AgentWebsocketConfig(private val eventPublisher: EventPublisher) {

    private val handleAgentEvents: WebSocketHandler = WebSocketHandler { session ->
        session.receive()
                .map { Event.parseFrom(it.payload.asInputStream()) }
                .transform(eventPublisher::publish)
                .then()
    }

    @Bean
    fun handlerMapping(): HandlerMapping =
            SimpleUrlHandlerMapping(mapOf(BASE_PATH to handleAgentEvents)).apply { order = Ordered.HIGHEST_PRECEDENCE }

    @Bean
    fun handlerAdapter(): WebSocketHandlerAdapter = WebSocketHandlerAdapter()

    companion object {
        private const val BASE_PATH = "/api/ws/agent"
    }
}

