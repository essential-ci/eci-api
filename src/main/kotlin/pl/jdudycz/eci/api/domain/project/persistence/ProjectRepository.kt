package pl.jdudycz.eci.api.domain.project.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface ProjectRepository : ReactiveCrudRepository<Project, String> {

    fun findBySpaceId(spaceId: String): Flux<Project>
}
