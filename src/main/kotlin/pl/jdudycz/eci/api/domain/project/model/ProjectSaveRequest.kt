package pl.jdudycz.eci.api.domain.project.model

import org.bson.internal.Base64
import pl.jdudycz.eci.common.domain.Project.ProjectData
import pl.jdudycz.eci.common.util.asEnvString

data class ProjectSaveRequest(
        val name: String,
        val spaceId: String,
        val repoUrl: String,
        val token: String?,
        val variables: Map<String, Any>?,
        val secretVariables: Map<String, Any>?
) {

    fun toCommandData(id: String?, encryptionProvider: (String) -> String): ProjectData =
            ProjectData.newBuilder()
                    .setId(id ?: "")
                    .setName(name)
                    .setSpaceId(spaceId)
                    .setRepoUrl(repoUrl)
                    .setIsPrivate(token != null)
                    .setToken(token?.let(::decodeBase64)?.let(encryptionProvider) ?: "")
                    .addAllVariables(variables?.asEnvString() ?: emptyList())
                    .addAllSecretVariables(encrypt(secretVariables, encryptionProvider))
                    .build()


    private fun encrypt(secretVariables: Map<String, Any>?,
                        encryptionProvider: (String) -> String): List<String> =
            secretVariables
                    ?.mapValues { encryptionProvider(decodeBase64(it.value)) }
                    ?.asEnvString() ?: emptyList()

    private fun decodeBase64(value: Any): String = Base64.decode(value.toString()).decodeToString()
}
