package pl.jdudycz.eci.api.domain.project.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import pl.jdudycz.eci.api.security.authorization.ParamExtractor
import pl.jdudycz.eci.api.security.authorization.RoleAccessManager
import pl.jdudycz.eci.api.domain.space.authorization.SpaceMembershipResolver

@Service
class ProjectAuthorizer(private val spaceMembershipResolver: SpaceMembershipResolver,
                        private val roleAuthorizer: RoleAccessManager) {

    fun authorizeRead(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.DEVELOPER,
                    spaceMembershipResolver::getProjectSpace,
                    PROJECT_ID_VARIABLE
            )

    fun authorizeWrite(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.ADMIN,
                    spaceMembershipResolver::getProjectSpace,
                    PROJECT_ID_VARIABLE
            )

    companion object {
        private const val PROJECT_ID_VARIABLE = "projectId"
    }
}
