package pl.jdudycz.eci.api.domain.space.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import pl.jdudycz.eci.api.security.authorization.ParamExtractor
import pl.jdudycz.eci.api.security.authorization.RoleAccessManager
import reactor.kotlin.core.publisher.toMono

@Service
class SpaceAuthorizer(private val roleAuthorizer: RoleAccessManager) {
    fun authorizeRead(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.DEVELOPER,
                    { it.toMono() },
                    SPACE_ID_VARIABLE
            )

    fun authorizeWrite(extractor: ParamExtractor) =
            roleAuthorizer.verifyAccess(
                    extractor,
                    SpaceRole.ADMIN,
                    { it.toMono() },
                    SPACE_ID_VARIABLE
            )

    companion object {
        private const val SPACE_ID_VARIABLE = "spaceId"
    }
}
