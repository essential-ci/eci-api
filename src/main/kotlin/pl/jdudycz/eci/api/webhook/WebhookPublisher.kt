package pl.jdudycz.eci.api.webhook

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.common.domain.Repo
import pl.jdudycz.eci.common.domain.event.repoChanged
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class WebhookPublisher(private val publisher: EventPublisher) {

    fun publish(change: Mono<Repo.ChangeData>): MonoResponse<String> =
            change
                    .map(::repoChanged)
                    .transform(publisher::publish)
                    .map { ResponseEntity.ok().build<String>() }
                    .onErrorResume { ResponseEntity.badRequest().body(it.localizedMessage).toMono() }

}
