package pl.jdudycz.eci.api.webhook.github

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.jdudycz.eci.api.common.util.MonoResponse
import pl.jdudycz.eci.api.webhook.WebhookPublisher
import pl.jdudycz.eci.api.webhook.gitlab.GitlabWebhookController
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/webhook/github")
class GithubWebhookController(private val publisher: WebhookPublisher) {

    @PostMapping
    fun receive(@RequestBody request: GithubWebhookRequest): MonoResponse<String> =
            Mono.fromCallable(request::toChangeData)
                    .doOnNext { log.debug("Received GitHub webhook job request for repo ${request.repo.url}") }
                    .transform(publisher::publish)

    companion object{
        private val log = logger<GitlabWebhookController>()
    }
}
