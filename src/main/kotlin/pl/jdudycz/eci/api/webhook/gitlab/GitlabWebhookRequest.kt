package pl.jdudycz.eci.api.webhook.gitlab

import com.fasterxml.jackson.annotation.JsonProperty
import pl.jdudycz.eci.common.domain.Repo

data class GitlabWebhookRequest(
        val ref: String,
        @JsonProperty("checkout_sha")
        val checkoutSha: String,
        @JsonProperty("repository")
        val repo: RepositoryData,
        @JsonProperty("object_kind")
        val triggerType: String) {

    data class RepositoryData(
            @JsonProperty("git_http_url")
            val url: String
    )

    fun toChangeData(): Repo.ChangeData =
            Repo.ChangeData.newBuilder()
                    .setCheckoutSha(checkoutSha)
                    .setRefValue(ref)
                    .setRepoUrl(repo.url)
                    .setType(resolveType(triggerType))
                    .build()

    private fun resolveType(objectKind: String) = when (objectKind) {
        "push" -> Repo.TriggerType.BRANCH
        "tag_push" -> Repo.TriggerType.TAG
        else -> throw Exception("Invalid object kind")
    }
}

