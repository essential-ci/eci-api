package pl.jdudycz.eci.api.webhook.github

import com.fasterxml.jackson.annotation.JsonProperty
import pl.jdudycz.eci.common.domain.Repo

data class GithubWebhookRequest(
        val ref: String,
        @JsonProperty("after")
        val checkoutSha: String,
        @JsonProperty("repository")
        val repo: RepositoryData) {

    data class RepositoryData(
            @JsonProperty("clone_url")
            val url: String
    )

    fun toChangeData(): Repo.ChangeData =
            Repo.ChangeData.newBuilder()
                    .setCheckoutSha(checkoutSha)
                    .setRefValue(ref)
                    .setRepoUrl(repo.url)
                    .setType(resolveType(ref))
                    .build()

    private fun resolveType(ref: String): Repo.TriggerType =
            when {
                ref.contains("refs/heads") -> Repo.TriggerType.BRANCH
                ref.contains("refs/tags") -> Repo.TriggerType.TAG
                else -> throw Exception("Invalid object kind")
            }
}

