package pl.jdudycz.eci.api.common.util

import arrow.core.Either
import reactor.core.publisher.Mono

typealias MonoEither<K, T> = Mono<Either<K, T>>
