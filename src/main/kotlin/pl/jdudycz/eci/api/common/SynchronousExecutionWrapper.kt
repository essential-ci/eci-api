package pl.jdudycz.eci.api.common

import arrow.core.Either
import pl.jdudycz.eci.api.common.util.MonoEither
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.toUUID
import reactor.core.publisher.Mono
import reactor.core.publisher.ReplayProcessor
import java.util.*
import javax.annotation.PostConstruct


abstract class SynchronousExecutionWrapper(private val publisher: EventPublisher,
                                           private val successConsumer: EventConsumer,
                                           private val failureConsumer: EventConsumer) {

    private val successInbound = ReplayProcessor.create<Event>()
    private val failureInbound = ReplayProcessor.create<Event>()

    @PostConstruct
    fun init() {
        successConsumer.consume().subscribe(successInbound::onNext)
        failureConsumer.consume().subscribe(failureInbound::onNext)
    }

    protected fun publishCommand(event: Mono<Event>): MonoEither<String, String> =
            publisher.publish(event).flatMap { listenForResult(it.toUUID()) }

    private fun listenForResult(correlationId: UUID): MonoEither<String, String> =
            Mono.first(listenForSuccess(correlationId), listenForFailure(correlationId))

    private fun listenForSuccess(correlationId: UUID): MonoEither<Nothing, String> =
            successInbound.consumeRelated(correlationId)
                    .map(::parseSuccessResult)
                    .next()

    protected abstract fun parseSuccessResult(event: Event): Either<Nothing, String>

    private fun listenForFailure(correlationId: UUID): MonoEither<String, Nothing> =
            failureInbound.consumeRelated(correlationId)
                    .map(::parseFailureResult)
                    .next()

    protected abstract fun parseFailureResult(event: Event): Either<String, Nothing>

    private fun ReplayProcessor<Event>.consumeRelated(correlationId: UUID) =
            filter { it.correlationId.toUUID() == correlationId }
}
