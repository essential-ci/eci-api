package pl.jdudycz.eci.api.common.util

import arrow.core.Either
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

typealias MonoResponse<T> = Mono<ResponseEntity<T>>

fun Either<String, String>.asDefaultResponse() =
        fold({ ResponseEntity.badRequest().body(it) }, { ResponseEntity.ok(it) })

