package pl.jdudycz.eci.api.common

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.util.encryption.TextEncryptor

@Configuration
class EncryptorConfig {

    @Bean
    fun textEncryptor(@Value("\${pl.jdudycz.eci.api.encryption.password}")
                      password: String,
                      @Value("\${pl.jdudycz.eci.api.encryption.salt}")
                      salt: String): TextEncryptor = TextEncryptor(password, salt)
}
