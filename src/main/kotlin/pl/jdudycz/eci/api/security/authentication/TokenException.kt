package pl.jdudycz.eci.api.security.authentication

import org.springframework.security.core.AuthenticationException

class TokenException(msg: String, t: Throwable) : AuthenticationException(msg, t)
