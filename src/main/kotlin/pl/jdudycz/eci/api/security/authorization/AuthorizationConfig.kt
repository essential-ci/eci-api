package pl.jdudycz.eci.api.security.authorization

import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec

interface AuthorizationConfig {
    fun configureAccess(spec: AuthorizeExchangeSpec): AuthorizeExchangeSpec
}
