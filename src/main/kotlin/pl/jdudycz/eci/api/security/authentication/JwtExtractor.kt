package pl.jdudycz.eci.api.security.authentication

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter
import org.springframework.stereotype.Service
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Service
class JwtExtractor : ServerAuthenticationConverter {

    override fun convert(exchange: ServerWebExchange): Mono<Authentication> =
            Mono.justOrEmpty(exchange.request.headers[AUTH_HEADER])
                    .filter { it.isNotEmpty() }
                    .map { it.first().substringAfter(BEARER_PREFIX) }
                    .map { UsernamePasswordAuthenticationToken(it, it) }

    companion object {
        private const val AUTH_HEADER = "Authorization"
        private const val BEARER_PREFIX = "Bearer "
    }

}
