package pl.jdudycz.eci.api.security.authentication

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseToken
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class FirebaseAuthManager(private val firebaseAuth: FirebaseAuth) : ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication): Mono<Authentication> =
            Mono.fromCallable { validate(authentication) }
                    .onErrorMap { TokenException("Failed to validate token", it) }
                    .map {
                        UsernamePasswordAuthenticationToken(
                                it.uid,
                                authentication.credentials,
                                emptyList()
                        )
                    }

    private fun validate(authentication: Authentication): FirebaseToken =
            firebaseAuth.verifyIdToken(authentication.credentials as String)

}
