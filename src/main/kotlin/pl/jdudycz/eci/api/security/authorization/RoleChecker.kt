package pl.jdudycz.eci.api.security.authorization

import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import pl.jdudycz.eci.api.domain.space.model.SpaceRoleMapping
import pl.jdudycz.eci.api.domain.space.persistence.SpaceRoleMappingRepository
import reactor.core.publisher.Mono

@Service
class RoleChecker(private val roleMappingRepository: SpaceRoleMappingRepository) {

    fun hasAdminRole(userId: String, spaceId: String): Mono<Boolean> = hasRole(userId, spaceId, SpaceRole.ADMIN)

    fun hasDeveloperRole(userId: String, spaceId: String): Mono<Boolean> = hasRole(userId, spaceId, SpaceRole.DEVELOPER)

    fun hasRole(userId: String, spaceId: String, role: SpaceRole): Mono<Boolean> =
            roleMappingRepository.findByUserIdAndSpaceId(userId, spaceId)
                    .map(SpaceRoleMapping::role)
                    .collectList()
                    .map { roles -> roles.any { it.priority <= role.priority } }
}
