package pl.jdudycz.eci.api.security.authorization

import org.springframework.security.web.server.authorization.AuthorizationContext
import reactor.core.publisher.Mono

typealias ParamExtractor = (context: AuthorizationContext, variableName: String) -> Mono<String>

fun variableExtractor(context: AuthorizationContext, variableName: String): Mono<String> =
        Mono.justOrEmpty(context.variables[variableName] as String?)

fun queryParamExtractor(context: AuthorizationContext, variableName: String): Mono<String> =
        Mono.justOrEmpty(context.exchange.request.queryParams.getFirst(variableName))
