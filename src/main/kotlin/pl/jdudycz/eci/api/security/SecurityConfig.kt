package pl.jdudycz.eci.api.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import pl.jdudycz.eci.api.security.authentication.FirebaseAuthManager
import pl.jdudycz.eci.api.security.authentication.JwtExtractor
import pl.jdudycz.eci.api.security.authorization.AuthorizationConfig

@Configuration
class SecurityConfig(private val authorizationConfig: List<AuthorizationConfig>) {


    @Bean
    fun securityFilterChain(httpSecurity: ServerHttpSecurity,
                            firebaseAuthManager: FirebaseAuthManager,
                            jwtExtractor: JwtExtractor): SecurityWebFilterChain {

        val authFilter = AuthenticationWebFilter(firebaseAuthManager)
                .apply { setServerAuthenticationConverter(jwtExtractor) }

        return authorizationConfig
                .fold(httpSecurity.authorizeExchange()) { acc, config -> config.configureAccess(acc) }
                .pathMatchers("/api/**").authenticated()
                .anyExchange().permitAll()
                .and()
                .addFilterAt(authFilter, SecurityWebFiltersOrder.AUTHENTICATION)
                .httpBasic().disable()
                .csrf().disable()
                .cors().disable()
                .formLogin().disable()
                .logout().disable()
                .build()
    }
}
