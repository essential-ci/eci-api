package pl.jdudycz.eci.api.security.authorization

import org.springframework.security.authorization.AuthorizationDecision
import org.springframework.security.core.Authentication
import org.springframework.security.web.server.authorization.AuthorizationContext
import org.springframework.stereotype.Service
import pl.jdudycz.eci.api.domain.space.model.SpaceRole
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class RoleAccessManager(private val roleChecker: RoleChecker) {

    fun verifyAccess(extractor: ParamExtractor,
                     role: SpaceRole,
                     spaceIdMapper: (String) -> Mono<String>,
                     paramName: String) = { authentication: Mono<Authentication>, context: AuthorizationContext ->
        authentication
                .map { it.principal as String }
                .flatMap { userId ->
                    extractor(context, paramName)
                            .flatMap { spaceIdMapper(it) }
                            .flatMap { roleChecker.hasRole(userId, it, role) }
                            .map(::AuthorizationDecision)
                            .switchIfEmpty(AuthorizationDecision(false).toMono())
                }
    }
}
